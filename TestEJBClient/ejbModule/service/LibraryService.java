/**
 * LibraryService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package service;

public interface LibraryService extends java.rmi.Remote {
    public service.Response getMap() throws java.rmi.RemoteException;
    public void addBook(service.Book bookName) throws java.rmi.RemoteException;
    public service.Book[] getBooks() throws java.rmi.RemoteException;
    public service.Book getBookName(java.lang.String name) throws java.rmi.RemoteException;
}
