/**
 * LibraryPersistentBeanService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package service;

public interface LibraryPersistentBeanService extends javax.xml.rpc.Service {
    public java.lang.String getLibraryServicePortAddress();

    public service.LibraryService getLibraryServicePort() throws javax.xml.rpc.ServiceException;

    public service.LibraryService getLibraryServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
