package service;

public class LibraryServiceProxy implements service.LibraryService {
  private String _endpoint = null;
  private service.LibraryService libraryService = null;
  
  public LibraryServiceProxy() {
    _initLibraryServiceProxy();
  }
  
  public LibraryServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initLibraryServiceProxy();
  }
  
  private void _initLibraryServiceProxy() {
    try {
      libraryService = (new service.LibraryPersistentBeanServiceLocator()).getLibraryServicePort();
      if (libraryService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)libraryService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)libraryService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (libraryService != null)
      ((javax.xml.rpc.Stub)libraryService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public service.LibraryService getLibraryService() {
    if (libraryService == null)
      _initLibraryServiceProxy();
    return libraryService;
  }
  
  public service.Response getMap() throws java.rmi.RemoteException{
    if (libraryService == null)
      _initLibraryServiceProxy();
    return libraryService.getMap();
  }
  
  public void addBook(service.Book bookName) throws java.rmi.RemoteException{
    if (libraryService == null)
      _initLibraryServiceProxy();
    libraryService.addBook(bookName);
  }
  
  public service.Book[] getBooks() throws java.rmi.RemoteException{
    if (libraryService == null)
      _initLibraryServiceProxy();
    return libraryService.getBooks();
  }
  
  public service.Book getBookName(java.lang.String name) throws java.rmi.RemoteException{
    if (libraryService == null)
      _initLibraryServiceProxy();
    return libraryService.getBookName(name);
  }
  
  
}