/**
 * LibraryPersistentBeanServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package service;

public class LibraryPersistentBeanServiceLocator extends org.apache.axis.client.Service implements service.LibraryPersistentBeanService {

    public LibraryPersistentBeanServiceLocator() {
    }


    public LibraryPersistentBeanServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LibraryPersistentBeanServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LibraryServicePort
    private java.lang.String LibraryServicePort_address = "http://localhost:7001/LibraryPersistentBean/LibraryPersistentBeanService";

    public java.lang.String getLibraryServicePortAddress() {
        return LibraryServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LibraryServicePortWSDDServiceName = "LibraryServicePort";

    public java.lang.String getLibraryServicePortWSDDServiceName() {
        return LibraryServicePortWSDDServiceName;
    }

    public void setLibraryServicePortWSDDServiceName(java.lang.String name) {
        LibraryServicePortWSDDServiceName = name;
    }

    public service.LibraryService getLibraryServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LibraryServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLibraryServicePort(endpoint);
    }

    public service.LibraryService getLibraryServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            service.LibraryServicePortBindingStub _stub = new service.LibraryServicePortBindingStub(portAddress, this);
            _stub.setPortName(getLibraryServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLibraryServicePortEndpointAddress(java.lang.String address) {
        LibraryServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (service.LibraryService.class.isAssignableFrom(serviceEndpointInterface)) {
                service.LibraryServicePortBindingStub _stub = new service.LibraryServicePortBindingStub(new java.net.URL(LibraryServicePort_address), this);
                _stub.setPortName(getLibraryServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LibraryServicePort".equals(inputPortName)) {
            return getLibraryServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("service", "LibraryPersistentBeanService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("service", "LibraryServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LibraryServicePort".equals(portName)) {
            setLibraryServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
