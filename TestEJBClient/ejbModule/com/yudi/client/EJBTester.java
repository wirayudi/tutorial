package com.yudi.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import com.yudi.bean.LibraryPersistentBean;
import com.yudi.model.Book;
import com.yudi.remote.LibraryPersistentBeanRemote;

public class EJBTester {

	BufferedReader brConsoleReader = null;
	Context ctx;
	{

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"weblogic.jndi.WLInitialContextFactory");
		env.put(Context.SECURITY_PRINCIPAL, "weblogic");
		env.put(Context.SECURITY_CREDENTIALS, "weblogic1");
		env.put(Context.PROVIDER_URL, "t3://localhost:7001");

		try {
			ctx = new InitialContext(env);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		brConsoleReader = new BufferedReader(new InputStreamReader(System.in));
	}

	public EJBTester() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {

		EJBTester test = new EJBTester();
		test.testEntityEjb();

	}

	private void showGUI() {
		System.out.println("**********************");
		System.out.println("Welcome to Book Store");
		System.out.println("**********************");
		System.out.print("Options \n1. Add Book\n2. Exit \nEnter Choice: ");
	}

	private void testEntityEjb() {
		try {
			int choice = 1;
			Object obj = ctx.lookup("LibraryPersistentBeanRemote#com.yudi.remote.LibraryPersistentBeanRemote");
			//LibraryPersistentBeanRemote libraryBean = (LibraryPersistentBeanRemote) ctx.lookup("LibraryPersistentBeanRemote#com.yudi.remote.LibraryPersistentBeanRemote");
			
			LibraryPersistentBeanRemote libraryBean = (LibraryPersistentBeanRemote) PortableRemoteObject.narrow(obj, LibraryPersistentBeanRemote.class);
			
			while (choice != 2) {
				String bookName;
				showGUI();
				String strChoice = brConsoleReader.readLine();
				choice = Integer.parseInt(strChoice);
				if (choice == 1) {
					System.out.print("Enter book name: ");
					bookName = brConsoleReader.readLine();
					Book book = new Book();
					book.setName(bookName);
					libraryBean.addBook(book);
				} else if (choice == 2) {
					break;
				}
			}
			List<Book> booksList = libraryBean.getBooks();
			System.out.println("Book(s) entered so far: " + booksList.size());
			int i = 0;
			for (Book book : booksList) {
				System.out.println((i + 1) + ". " + book.getName());
				i++;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (brConsoleReader != null) {
					brConsoleReader.close();
				}
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
}