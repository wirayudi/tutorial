package com.yudi.client;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import com.yudi.model.Book;

import service.LibraryPersistentBeanServiceLocator;
import service.LibraryServicePortBindingStub;
import service.MapElements;
import service.Response;

public class TestWebService {

	public static void main(String[] args) throws ServiceException,
			RemoteException {

		LibraryPersistentBeanServiceLocator locator = new LibraryPersistentBeanServiceLocator();
		LibraryServicePortBindingStub stub = (LibraryServicePortBindingStub) locator
				.getLibraryServicePort();

		List<service.Book> books = Arrays.asList(stub.getBooks());

		for (service.Book b : books) {
			System.out.println("Result : " + b.getName());
		}

/*		service.Book book = new service.Book();
		book.setName("Wira");
		stub.addBook(book);

		books = Arrays.asList(stub.getBooks());

		for (service.Book b : books) {
			System.out.println("Result : " + b.getName());
		}
*/		
			
		Response mapObject = stub.getMap();
		MapElements[] obj = mapObject.getMapProperty();
		for(MapElements m : obj){
			System.out.println("Key :" + m.getKey() + " Value : "+ m.getValue());			
		}
		
		service.Book book = stub.getBookName("wira");
		System.out.println(book.getName());
		
	}
}
