package com.yudi.remote;

import java.util.List;

import javax.ejb.Remote;

import com.yudi.model.Book;
import com.yudi.soap.handler.Response;

@Remote
public interface LibraryPersistentBeanRemote {

	void addBook(Book bookName);	
	List<Book> getBooks();
	Response getMap();
	Book getBookName(String name);	
}
