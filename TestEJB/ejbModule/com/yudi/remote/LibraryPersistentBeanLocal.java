package com.yudi.remote;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import com.yudi.model.Book;
import com.yudi.soap.handler.Response;

@Local
@WebService
@SOAPBinding(style=Style.DOCUMENT)
public interface LibraryPersistentBeanLocal{

	@WebMethod void addBook(Book bookName);	
	@WebMethod List<Book> getBooks();
	@WebMethod Response getMap();
	@WebMethod Book getBookName(String name);	
	
}
