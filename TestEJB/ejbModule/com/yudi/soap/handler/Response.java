package com.yudi.soap.handler;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

public class Response {

	private Map<Object, Object> mapProperty;

	public Response() {
		// TODO Auto-generated constructor stub
		mapProperty = new HashMap<Object, Object>();
		
	}
	
	@XmlJavaTypeAdapter(MapAdapter.class)
	public Map<Object, Object> getMapProperty() {
		return mapProperty;
	}

	public void setMapProperty(Map<Object, Object> map) {
        this.mapProperty = map;
    }
}
