package com.yudi.soap.handler;

import javax.xml.bind.annotation.XmlElement;

public class MapElements {

	@XmlElement
	public Object key;
	@XmlElement
	public Object value;

	private MapElements() {
	} // Required by JAXB

	public MapElements(Object key, Object value) {
		this.key = key;
		this.value = value;
	}

}
