package com.yudi.soap.handler;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MapAdapter extends XmlAdapter<MapElements[], Map<Object, Object>> {

	@Override
	public Map<Object, Object> unmarshal(MapElements[] arg0) throws Exception {
		// TODO Auto-generated method stub
		  Map<Object, Object> r = new HashMap<Object, Object>();
	        for (MapElements mapelement : arg0)
	            r.put(mapelement.key, mapelement.value);
	        return r;
	}

	@Override
	public MapElements[] marshal(Map<Object, Object> arg0) throws Exception {
		// TODO Auto-generated method stub
		MapElements[] mapElements = new MapElements[arg0.size()];
        int i = 0;
        for (Map.Entry<Object, Object> entry : arg0.entrySet())
            mapElements[i++] = new MapElements(entry.getKey(), entry.getValue());

        return mapElements;
	}


}
