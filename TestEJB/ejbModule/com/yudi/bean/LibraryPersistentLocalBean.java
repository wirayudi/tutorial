package com.yudi.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.yudi.model.Book;
import com.yudi.remote.LibraryPersistentBeanLocal;
import com.yudi.soap.handler.Response;

@Stateless(name="LibraryPersistentLocalBean", mappedName="LibraryPersistentBeanLocal")
@WebService(name="LibraryService", targetNamespace="service")
@SOAPBinding(style = Style.DOCUMENT)
@HandlerChain(file="handler-chain.xml")
public class LibraryPersistentLocalBean implements LibraryPersistentBeanLocal{

	@PersistenceContext(unitName="EjbComponentPU")
	private EntityManager entityManager;	
	
	@WebMethod(operationName="addBook")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addBook(@WebParam(name="bookName")Book bookName) {
		// TODO Auto-generated method stub
		entityManager.persist(bookName);
		/*
		Connection conn = null;
		String url ="jdbc:mysql://localhost/test";
		String driver="com.mysql.jdbc.Driver";
		String user="root";
		String pass="password";
		
		List<Book> books = new ArrayList<Book>();
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, user, pass);			
			
			PreparedStatement ps = conn.prepareStatement("insert into book(name) values(?)");
			ps.setString(1, bookName.getName());
			
			int rs = ps.executeUpdate();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		*/
		
	}

	@SuppressWarnings("unchecked")
	@WebMethod(operationName="getBooks")
	public List<Book> getBooks() {
		// TODO Auto-generated method stub
		return entityManager.createQuery("select b from Book b").getResultList();
		//return entityManager.createQuery("from Book").getResultList();
		/*
		Connection conn = null;
		String url ="jdbc:mysql://localhost/test";
		String driver="com.mysql.jdbc.Driver";
		String user="root"; 
		String pass="password";
		
		List<Book> books = new ArrayList<Book>();
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, user, pass);			
			
			Statement ps = conn.createStatement();			
			
			ResultSet rs = ps.executeQuery("select * from book");
			Book book;
			while (rs.next()) {				
				book = new Book();
				book.setId(rs.getInt(1));
				book.setName(rs.getString(2));
				books.add(book);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}*/
		
	}

	@Override
	@WebMethod(operationName="getMap")	
	public Response getMap() {
		// TODO Auto-generated method stub
		Map<Object, Object> map = new HashMap<Object, Object>();
				
		map.put("SATU", 10.5);
		map.put("DUA", 450);
		Response resp = new Response();
		
		resp.setMapProperty(map);
		return resp;
	}

	@Override
	@WebMethod
	public Book getBookName(@WebParam(name="name")String name) {
		// TODO getBookName Method
		String sql = "select b from Book b where b.name = ?1";
		//String sql = "from Book b where b.name = :name";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, name);
		//query.setParameter("name", name);
				
		return (Book) query.getSingleResult();
	}


	
	
}
