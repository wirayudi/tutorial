package com.yudi.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.yudi.model.Book;
import com.yudi.remote.LibraryPersistentBeanRemote;
import com.yudi.soap.handler.Response;


@Stateless(name="LibraryPersistentRemoteBean", mappedName="LibraryPersistentBeanRemote")
public class LibraryPersistentRemoteBean implements LibraryPersistentBeanRemote{

	@PersistenceContext(unitName="EjbComponentPU")
	private EntityManager entityManager;	
	
	@Override
	public void addBook(Book bookName) {
		// TODO Auto-generated method stub
		entityManager.persist(bookName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> getBooks() {
		// TODO Auto-generated method stub
		return entityManager.createQuery("select b from Book b").getResultList();
	}

	@Override
	public Response getMap() {
		// TODO Auto-generated method stub
		Map<Object, Object> map = new HashMap<Object, Object>();
		
		map.put("SATU", 10.5);
		map.put("DUA", 450);
		Response resp = new Response();
		
		resp.setMapProperty(map);
		return resp;
	}

	@Override
	public Book getBookName(String name) {
		// TODO Auto-generated method stub
		String sql = "select b from Book b where b.name = ?1";
		//String sql = "from Book b where b.name = :name";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, name);
		//query.setParameter("name", name);
				
		return (Book) query.getSingleResult();
	}

	
}
